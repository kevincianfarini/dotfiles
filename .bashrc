# .bashrc
__git_ps1() { git branch 2>/dev/null | sed -n 's/\* \(.*\)/ (\1)/p'; }

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

# added by Anaconda3 installer
alias python='python3'

export PS1='\[\e[1;32m\][\u@\h \w]\[\e[1;34m\]$(__git_ps1) \$\[\e[0m\] '

alias anaconda-navigator='source /home/kevin/anaconda3/bin/activate root && anaconda-navigator'
