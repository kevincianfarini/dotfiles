# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin
export PS1="\[\e[1;32m\][\u@\h \w]\e[m\e[1;34m\$\[\e[0m\] "

export PATH
